Views Node Filter


Description:
========================
Views provides us with abundance of filters; however, Views doesn't provide a
list of nodes as a filterfor this reason. This module solves that issue by,
well, using Views!

Views Node Filter obtains the result of a view and allows you to use it as a
filter in another view / display (although you can still end up with the
above issue if you create a view for a filter that returns thousands of
results).


How to set up:
========================
Here's how to create a page where you can use an exposed node filter to
filter the content (this is written with an assumption that the readers
know how to use Views):

1. Install and enable Views and Views Node Filter.

2. First, create a view that returns a list of node IDs. The result of#
this view will be used as a filter in another view.

3. In 'Fields', add a 'nid' field (you can add other fields, but they
will be ignored). Save the view.

4. Create another view (or a display) for the actual page view. Create a
page display and specify the path.

5. In 'Filters', select 'Views Node Filter' and tick 'nid'. In the
configuration box that opens below (if you have JavaScript enabled),
select the view and display created in step #2.

6. Expose the filter and click 'Update and override' or 'Update default
display'.

7. Save the view and open the URL to the page view.



How it works:
========================
The module obtains a list of nodes from a view / display you specify and uses
the list as a filter. As the module executes a view to obtain the list, it
calls $view->set_items_per_page(0) so the filter will return all the nodes in
the result regardless of the value given in 'items to display'.


Credit:
========================
- Development of this module was sponsored by Comic Relief UK
(http://www.comicrelief.com/ )

- Developed by Mori Sugimoto (http://drupal.org/user/82971)

Disclaimer:
========================
As always, use it at your own risk. If you encounter any issue, please
report through the issue queue. The developer or the sponsor cannot be
held accountable for any of the damages which the module may cause.
