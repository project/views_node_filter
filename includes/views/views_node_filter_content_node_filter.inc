<?php
/**
 * @file
 * This provides a filter with extra options.
 */
class views_node_filter_content_node_filter extends views_handler_filter_many_to_one {

  function has_extra_options() { return TRUE; }

  function option_definition() {
    $options = parent::option_definition();
    // add a custom option definition
    $options['view_display'] = array('default' => '');

    // Limit the exposed filter item
    //$options['limit'] = array('default' => TRUE);

    return $options;
  }

  function get_value_options() {  /* don't override this value */  }

  // The extra options form is displayed when the cog icon is clicked
  function extra_options_form(&$form, &$form_state) {
    // get a list of views and their displays
    // TODO: present this in a better way
    $view_displays = views_node_filter_view_displays();
    // option definition value is set by this form
    $form['view_display'] = array(
      '#prefix' => '<div class="views-left-80">',
      '#suffix' => '</div>',
      '#type' => 'radios',
      '#title' => t('Views Displays'),
      '#options' => $view_displays,
      '#description' => t('Select which view\'s results to use for the filter.'),
      '#default_value' => $this->options['view_display'],
    );
  }

  // This form returns a list of nodes.
  // The condition is as specified by the view selected in the extra option
  function value_form(&$form, &$form_state) {

    // Get the view name and display name
    $filter_view = explode('|', $this->options['view_display']);

    // Load the view using the option value saved in the extra option form
    $view = views_get_view($filter_view[0]);

    // Set the display
    $view->set_display($filter_view[1]);
    
    // force 'items to display' to be unlimited
    $view->set_items_per_page(0);
    $view->execute();
    

    // If the view returns no values, return a dummy value

    if (empty($view->result)) {
      $options = array('-1' => t('(This view returns no values)'));
    }
    // The view returns values - process normally
    else {
      // check if it has node_title
      // if it does, use it.
      // if not, get nids and get titles manually.
      $title_exists = FALSE;
      if (!empty($view->result[0]->node_title)) {
        $title_exists = TRUE;
      }

      //get the nids
      $options = array();
      $nids = array();
      foreach ($view->result as $result) {
        if ($title_exists !== FALSE) {
          //dsm($result->node_title);
          $options[$result->nid] = check_plain($result->node_title);
        }
        else {
          $nids[] = $result->nid;
        }
      }
      $count = count($nids);
      if (!empty($count)) {
        $options = views_node_filter_get_titles($nids);
      }
    }

    if (!empty($form_state['exposed'])) {
         $identifier = $this->options['expose']['identifier'];

      // If 'Limit to seleced items' is selected, return only the
      // selected items
      if (!empty($this->options['expose']['reduce'])) {
        $options = $this->reduce_value_options($options);

        if (empty($this->options['expose']['single']) && !empty($this->options['expose']['optional'])) {
          $default_value = array();
        }
      }
    }


   // This form contains a list of nodes
   // dsm($this->options);
    $form['value'] = array(
      '#type' => 'select',
      '#title' => t('Node title'),
      '#multiple' => TRUE,
      '#options' => $options,
      '#size' => min(9, count($options)),
      '#default_value' => $this->value,
    );
  }

  function value_submit($form, &$form_state) {
    // prevent array_filter from messing up our arrays in parent submit.
  }

  // function expose_form_right(&$form, &$form_state) {
  //   parent::expose_form_right($form, $form_state);
  // }

  // Displays a summary of selected nodes after the name of a filter
  function admin_summary() {
    $this->value_options = array();

//    if ($this->value) {
//      $result = db_query("SELECT nid, title FROM {node} n WHERE n.nid IN ("  . implode(', ', $this->value) . ")");
//
//      while ($node = db_fetch_object($result)) {
//        $this->value_options[$node->nid] = $node->title;
//      }
//    }

    if ($this->value) {

      $placeholders = db_placeholders($this->value, 'int');
      $sql = "SELECT nid, title FROM {node} WHERE nid IN ($placeholders)";
      $result = db_query($sql, $this->value);

      while ($node = db_fetch_object($result)) {
        $this->value_options[$node->nid] = check_plain($node->title);
      }
    }
    return parent::admin_summary();
  }
}

/**
 * Receives nids in an array and returns an array of options
 * to be used in a select list
 *
 * @param $nids
 *   An array of node IDs
 *
 * @return $options
 *   An array of node titles with the node IDs as keys
 */
function views_node_filter_get_titles($nids) {
  $options = array();
  if (!empty($nids)) {
    $placeholders = db_placeholders($nids, 'int');
    $sql = "SELECT nid, title FROM {node} WHERE nid IN ($placeholders)";
    $results = db_query($sql, $nids);

    // get the values from the results.
    // safe to assume at least one result exists
    while ($result = db_fetch_array($results)) {
      $options[$result['nid']] = check_plain($result['title']);
    }
  }
  return $options;
}

 /**
  * Returns a list of views' displays
  * @return $views_displays
  *   An array of view_displays whose keys are [view id|display id]
  */
function views_node_filter_view_displays() {
  // get a list of all views
//  // TODO: exclude for itself
  $sql = "SELECT vv.vid, vv.name, vd.id, vd.display_title FROM {views_view} vv
          INNER JOIN {views_display} vd ON vv.vid = vd.vid
          AND vd.id <> '%s'
          ORDER BY vv.name, vd.display_title";

  $results = db_query($sql);
  $view_displays = array();
  while ($result = db_fetch_array($results)) {
    $view_displays[$result['name'] ."|". $result['id']] = $result['name'] ." :: " . $result['display_title'];
  }
  return $view_displays;
}
