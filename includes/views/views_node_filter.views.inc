 <?php
function views_node_filter_views_handlers() {
  return array(
    'info' => array(
       'path' => drupal_get_path('module', 'views_node_filter') . '/includes/views',
    ),
    'handlers' => array(
      'views_node_filter_content_node_filter' => array(
        'parent' => 'views_handler_filter_many_to_one',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data().
 *
 * What we want in the end is just a simple query that filters nodes by nid.
 * However, Views requires modules to specify the table names it works with as
 * an array key, and there cannot be more than one module that uses a table in
 * the same way. As a result, this module would conflict with
 * views_handler_filter_node.inc.
 *
 * It's quite hacky, but for this reason I'm using a placeholder for a table
 * name, then implementing hook_views_query_alter() to replace it.
 * @return array $data
 */
function views_node_filter_views_data() {

  $data = array();
  // node table -- basic table information.

  // Define the base group of this table. Fields that don't
  // have a group defined will go into this field by default.
  $data['views_node_filter_placeholder']['table']['group']  = t('Views Node Filter');

  // Advertise this table as a possible base table
  $data['views_node_filter_placeholder']['table']['base'] = array(
  // Primary key must be used
    'field' => 'vid',
    'title' => t('Version ID'),
    'help' => t("Nodes are a Drupal site's primary content."),
  );

  // For other base tables, explain how we join
  $data['views_node_filter_placeholder']['table']['join'] = array(
    // this explains how the 'node' table (named in the line above)
    // links toward the node_revisions table.
    'node' => array(
      'handler' => 'views_join', // this is actually optional
      'left_table' => 'node', // Because this is a direct link it could be left out.
      'left_field' => 'vid',
      'field' => 'vid',
     ),
  );
//   
  $data['views_node_filter_placeholder']['nid'] = array(
    'title' => t('nid'), // The item it appears as on the UI,
    'help' => t('Node ID.'), // The help that appears on the UI,

    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    // Information for accepting a title as a filter
    'filter' => array(
      'handler' => 'views_node_filter_content_node_filter',
    ),
  );
  return $data;
}
